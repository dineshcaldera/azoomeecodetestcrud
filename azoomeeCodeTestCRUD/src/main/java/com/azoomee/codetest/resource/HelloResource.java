package com.azoomee.codetest.resource;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResource {

	@RequestMapping("/codetest/hello")
	public String hello() {
		return "Hello codetest CRUD! ";
	}
	
	
	
}
