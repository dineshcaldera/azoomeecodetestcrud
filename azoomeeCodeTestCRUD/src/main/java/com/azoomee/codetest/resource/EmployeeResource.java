package com.azoomee.codetest.resource;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.azoomee.codetest.domain.Employee;
import com.azoomee.codetest.service.EmployeeService;

/**
 * Resource Class for Employee CRUD operation.
 * 
 * @author dineshcaldera
 *
 */

@RestController
public class EmployeeResource {

	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value = "/codetest/employees", 
	produces = "application/json; charset=UTF-8",
	method = RequestMethod.GET)

	public @ResponseBody Collection<Employee> getAllEmployee() {
		return employeeService.findAll();
	}

	@RequestMapping(value = "/codetest/employees/{id}", 
			produces = "application/json; charset=UTF-8",
			method = RequestMethod.GET)
	
	//http://localhost:8080/codetest/employee/100
	
	public @ResponseBody Employee getAllEmployee(@PathVariable long id) {
		return employeeService.findOne(id);
	}

	
	@RequestMapping(value = "/codetest/employees", 
			produces = "application/json",
			consumes = "application/json",
			method = RequestMethod.POST)
	
	//http://localhost:8080/codetest/employee
	@ResponseStatus(value=HttpStatus.CREATED)
	public @ResponseBody Employee addEmployee(@RequestBody Employee employee) {

		return employeeService.create(employee);
	}

	@RequestMapping(value = "/codetest/employees/{id}", 
			produces = "application/json",
			consumes = "application/json",
			method = RequestMethod.PUT)
	
	//http://localhost:8080/codetest/employee/3
	public @ResponseBody Employee updateEmployee(@PathVariable long id, @RequestBody Employee employee) {

		return employeeService.update(employee);
	}
	
	
	@RequestMapping(value = "/codetest/employees/{id}", 
			produces = "application/json; charset=UTF-8",
			method = RequestMethod.DELETE)
	//http://localhost:8080/codetest/employee/3/delete
	public @ResponseBody void deleteEmployee(@PathVariable long id) {
		
		employeeService.delete(id);
	}
}
