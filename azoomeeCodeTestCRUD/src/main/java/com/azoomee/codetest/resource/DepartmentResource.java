package com.azoomee.codetest.resource;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.azoomee.codetest.domain.Department;
import com.azoomee.codetest.service.DepartmentService;


/**
 * Resource Class for Department CRUD operation.
 * 
 * @author dineshcaldera
 *
 */


@RestController
public class DepartmentResource {

	@Autowired
	DepartmentService departmentService;
	
	@RequestMapping(value = "/codetest/departments", 
	produces = "application/json; charset=UTF-8",
	method = RequestMethod.GET)

	public @ResponseBody Collection<Department> getAllDepartment() {
		return departmentService.findAll();
	}

	@RequestMapping(value = "/codetest/departments/{id}", 
			produces = "application/json; charset=UTF-8",
			method = RequestMethod.GET)
	
	//http://localhost:8080/codetest/departments/100
	
	public @ResponseBody Department getAllDepartment(@PathVariable long id) {
		return departmentService.findOne(id);
	}

	
	@RequestMapping(value = "/codetest/departments", 
			produces = "application/json",
			consumes = "application/json",
			method = RequestMethod.POST)
	
	//http://localhost:8080/codetest/departments
	@ResponseStatus(value=HttpStatus.CREATED)
	public @ResponseBody Department addDepartment(@RequestBody Department department) {
		//Department departmentToAdd = new Department();
		
	//	departmentToAdd.setId(department.getId());
	//	departmentToAdd.setName(department.getName());
		return departmentService.create(department);
	}

	@RequestMapping(value = "/codetest/departments/{id}", 
			produces = "application/json",
			consumes = "application/json",
			method = RequestMethod.PUT)
	
	public @ResponseBody Department updateDepartment(@PathVariable long id, @RequestBody Department department) {
		//Department departmentToAdd = new Department();
		
	//	departmentToAdd.setId(department.getId());
	//	departmentToAdd.setName(department.getName());
		
		return departmentService.update(department);
	}
	
	
	@RequestMapping(value = "/codetest/departments/{id}", 
			produces = "application/json; charset=UTF-8",
			method = RequestMethod.DELETE)
	public @ResponseBody void deleteDepartment(@PathVariable long id) {
		
		departmentService.delete(id);
	}
}
