package com.azoomee.codetest.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.azoomee.codetest.domain.Employee;


/**
 * Repository Class to for Employee entity.
 * 
 * @author dineshcaldera
 *
 */


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
