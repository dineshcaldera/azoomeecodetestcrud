package com.azoomee.codetest.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.azoomee.codetest.domain.Department;

/**
 * Repository Class to for Department entity.
 * 
 * @author dineshcaldera
 *
 */

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

}
