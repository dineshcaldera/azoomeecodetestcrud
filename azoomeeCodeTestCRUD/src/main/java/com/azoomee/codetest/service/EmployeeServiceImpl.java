package com.azoomee.codetest.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azoomee.codetest.domain.Employee;
import com.azoomee.codetest.domain.repository.EmployeeRepository;

/**
 * Class for Employee service implementation.
 * 
 * @author dineshcaldera
 *
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Employee create(Employee employee) {
		
		if (employee.getId() != null) {
			// not a new record 
			return null;
		}
		return employeeRepository.save(employee);
	}

	@Override
	public Employee update(Employee employee) {
		Employee employeeSaved = employeeRepository.findOne(employee.getId());
		
		if (employeeSaved == null) {
			//cannot update record
			return null;
		}
		return employeeRepository.save(employee);
	}

	@Override
	public Collection<Employee> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee findOne(Long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public void delete(Long id) {
		employeeRepository.delete(id);
		
	}
	
	
	
}


