package com.azoomee.codetest.service;

import java.util.Collection;

import com.azoomee.codetest.domain.Employee;

/**
 * Interface for Employee service.
 * 
 * @author dineshcaldera
 *
 */

public interface EmployeeService {
	
	public Employee create(Employee employee);
	
	public Employee update(Employee employee);
	
	public Collection<Employee> findAll();
	
	public Employee findOne(Long id);
	
	public void delete(Long id);
	
	
	

}
