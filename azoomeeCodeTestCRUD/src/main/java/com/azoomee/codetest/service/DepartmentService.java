package com.azoomee.codetest.service;

import java.util.Collection;

import com.azoomee.codetest.domain.Department;

/**
 * Interface for Department service.
 * 
 * @author dineshcaldera
 *
 */

public interface DepartmentService {
	
	public Department create(Department department);
	
	public Department update(Department department);
	
	public Collection<Department> findAll();
	
	public Department findOne(Long id);
	
	public void delete(Long id);
	
	
	

}
