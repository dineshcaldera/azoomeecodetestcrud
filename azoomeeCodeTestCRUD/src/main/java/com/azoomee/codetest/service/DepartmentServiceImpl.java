package com.azoomee.codetest.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azoomee.codetest.domain.Department;
import com.azoomee.codetest.domain.repository.DepartmentRepository;

/**
 * Class for Department service implementation.
 * 
 * @author dineshcaldera
 *
 */

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public Department create(Department department) {
		
		if (department.getId() != null) {
			// not a new record 
			return null;
		}
		return departmentRepository.save(department);
	}

	@Override
	public Department update(Department department) {
		Department departmentSaved = departmentRepository.findOne(department.getId());
		
		if (departmentSaved == null) {
			//cannot update record
			return null;
		}
		return departmentRepository.save(department);
	}

	@Override
	public Collection<Department> findAll() {
		return departmentRepository.findAll();
	}

	@Override
	public Department findOne(Long id) {
		return departmentRepository.findOne(id);
	}

	@Override
	public void delete(Long id) {
		departmentRepository.delete(id);
		
	}
	
	
	
}


