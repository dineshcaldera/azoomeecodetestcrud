package com.azoomee.codetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzoomeeCodeTestCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(AzoomeeCodeTestCrudApplication.class, args);
    }
}
