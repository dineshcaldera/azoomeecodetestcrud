package com.azoomee.codetest.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import static org.mockito.Mockito.verify;

import com.azoomee.codetest.domain.Department;
import com.azoomee.codetest.domain.repository.DepartmentRepository;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentServiceTest {


	@Mock
	DepartmentRepository departmentRepository;

	@InjectMocks
	DepartmentServiceImpl departmentService = new DepartmentServiceImpl();

	Department expected;
			
	@Before
	public void setUp() throws Exception {
	 expected = new Department();
	 expected.setId(1L);
	 expected.setName("IT");
	}

	@Test
	public void testCreate() {	
		expected.setId(null);
		when(departmentService.create(expected)).thenReturn(expected);
		
		Department actual = departmentService.create(expected);
		verify(departmentRepository).save(actual);
		assertNotNull(actual);
	}

	@Test
	public void testUpdate() {
		when(departmentRepository.findOne(expected.getId())).thenReturn(expected);
		when(departmentService.update(expected)).thenReturn(expected);
		
		Department actual = departmentService.update(expected);
		verify(departmentRepository).save(actual);
		assertNotNull(actual);

	}

	@Test
	public void testFindAll() {
		when(departmentRepository.findAll()).thenReturn(Arrays.asList(expected));
		
		Collection<Department> actual = departmentService.findAll();
		verify(departmentRepository).findAll();
		assertNotNull(actual);
		
	}

	@Test
	public void testFindOne() {
		when(departmentRepository.findOne(expected.getId())).thenReturn(expected);
		
		Department actual = departmentService.findOne(expected.getId());
		verify(departmentRepository).findOne(actual.getId());
		assertNotNull(actual);
	}

	@Test
	public void testDelete() {
		departmentService.delete(expected.getId());
		verify(departmentRepository).delete(expected.getId());
		assertNotNull(expected);
	}

}
