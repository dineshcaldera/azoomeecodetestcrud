package com.azoomee.codetest.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.azoomee.codetest.domain.Department;
import com.azoomee.codetest.domain.Employee;
import com.azoomee.codetest.domain.repository.EmployeeRepository;

import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
	
	
	@Mock
	EmployeeRepository employeeRepository;

	@InjectMocks
	EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

	Employee expected;

	@Before
	public void setUp() throws Exception {
		expected = new Employee();
		expected.setId(1L);
		expected.setFirstName("dinesh");
		expected.setLastName("caldera");
		

		Department expectedDept = new Department();
		expectedDept.setId(1L);
		expectedDept.setName("IT");
		
		expected.setDepartment(expectedDept);

	}

	@Test
	public void testCreate() {	
		expected.setId(null);
		when(employeeService.create(expected)).thenReturn(expected);
		
		Employee actual = employeeService.create(expected);
		verify(employeeRepository).save(actual);
		assertNotNull(actual);
	}

	@Test
	public void testUpdate() {
		when(employeeRepository.findOne(expected.getId())).thenReturn(expected);
		when(employeeService.update(expected)).thenReturn(expected);
		
		Employee actual = employeeService.update(expected);
		verify(employeeRepository).save(actual);
		assertNotNull(actual);

	}

	@Test
	public void testFindAll() {
		when(employeeRepository.findAll()).thenReturn(Arrays.asList(expected));
		
		Collection<Employee> actual = employeeService.findAll();
		verify(employeeRepository).findAll();
		assertNotNull(actual);
		
	}

	@Test
	public void testFindOne() {
		when(employeeRepository.findOne(expected.getId())).thenReturn(expected);
		
		Employee actual = employeeService.findOne(expected.getId());
		verify(employeeRepository).findOne(actual.getId());
		assertNotNull(actual);
	}

	@Test
	public void testDelete() {
		employeeService.delete(expected.getId());
		verify(employeeRepository).delete(expected.getId());
		assertNotNull(expected);
	}
}
